/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package App.Views;

import App.Models.Game;
import Datasets.DR;
import MYRV.DataTreatment.RcdMaker;
import MYRV.Navigation.Router;

/**
 *
 * @author MYRV
 */
public class Form extends javax.swing.JFrame {
    private Game game;
    /**
     * Creates new form Form
     */
    public Form() {
        initComponents();
        setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        LblName = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        LblConsoles = new javax.swing.JLabel();
        LblReviews = new javax.swing.JLabel();
        SpnScore = new javax.swing.JSpinner();
        BtnCancel = new javax.swing.JButton();
        BtnSave = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        GamesCRUD = new javax.swing.JMenu();
        Resports = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblName.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        LblName.setText("GameName");
        jPanel1.add(LblName, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 570, -1));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel1.setText("Puntaje:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, -1, -1));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel2.setText("Consolas:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, -1, -1));

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel3.setText("Reseñas:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, -1, -1));

        LblConsoles.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        LblConsoles.setText("LblConsolas");
        jPanel1.add(LblConsoles, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 40, 190, -1));

        LblReviews.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        LblReviews.setText("LblReseñas");
        jPanel1.add(LblReviews, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 70, 190, -1));

        SpnScore.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        SpnScore.setModel(new javax.swing.SpinnerNumberModel(0, 0, 10, 1));
        jPanel1.add(SpnScore, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 100, 40, -1));

        BtnCancel.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        BtnCancel.setText("Cancelar");
        BtnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelActionPerformed(evt);
            }
        });
        jPanel1.add(BtnCancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, -1, -1));

        BtnSave.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        BtnSave.setText("Guardar");
        BtnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSaveActionPerformed(evt);
            }
        });
        jPanel1.add(BtnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 140, -1, -1));

        jMenuBar1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        GamesCRUD.setText("Inicio");
        GamesCRUD.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        GamesCRUD.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                GamesCRUDMouseClicked(evt);
            }
        });
        GamesCRUD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GamesCRUDActionPerformed(evt);
            }
        });
        jMenuBar1.add(GamesCRUD);

        Resports.setText("Reportes");
        Resports.setToolTipText("Reportes de los juegos");
        Resports.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Resports.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ResportsActionPerformed(evt);
            }
        });
        jMenuBar1.add(Resports);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>                        

    private void GamesCRUDActionPerformed(java.awt.event.ActionEvent evt) {                                          
        Router.goTo("Index");
    }                                         

    private void ResportsActionPerformed(java.awt.event.ActionEvent evt) {                                         
        Router.goTo("Reports");
    }                                        

    private void GamesCRUDMouseClicked(java.awt.event.MouseEvent evt) {                                       
        Router.goTo("Index");
    }                                      

    private void BtnSaveActionPerformed(java.awt.event.ActionEvent evt) {                                        
        this.save();
        Router.goTo("Index");
    }                                       

    private void BtnCancelActionPerformed(java.awt.event.ActionEvent evt) {                                          
        Router.goTo("Index");
    }                                         

    public void mount(Game game) {
        this.game = game;
        LblName.setText(game.GameName());
        LblConsoles.setText(game.Console());
        LblReviews.setText(game.Review());
        SpnScore.setValue(game.Score());
    }
    
    public void save() {
        this.game = new Game(
            LblConsoles.getText(), 
            LblName.getText(), 
            LblReviews.getText(), 
            (Integer) SpnScore.getValue()
        );
        DR.Games.update(this.game, (rcd) -> {
            return ((Game) rcd).GameName().equals(this.game.GameName());
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton BtnCancel;
    private javax.swing.JButton BtnSave;
    private javax.swing.JMenu GamesCRUD;
    private javax.swing.JLabel LblConsoles;
    private javax.swing.JLabel LblName;
    private javax.swing.JLabel LblReviews;
    private javax.swing.JMenu Resports;
    private javax.swing.JSpinner SpnScore;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration                   
}
