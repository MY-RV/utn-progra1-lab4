/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App;

import App.Views.Form;
import App.Views.Index;
import App.Views.ReportChart;
import App.Views.ReportTable;
import MYRV.Config;
import MYRV.Navigation.Router;

/**
 *
 * @author MYRV
 */
public class Bootapp {
    public Bootapp() {
        Config.DataDir(Config.AppDir+"src/Datasets/Files/");
        Config.ViewsPackage("App.Views");
        Router.setViews(
            new Index(),
            new Form(),
            new ReportTable()
        );
    }
    
    public static void serve() {
        Router.goTo("Index");
    }
    
}