/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Record.java to edit this template
 */
package App.Models;

/**
 *
 * @author MYRV
 */
public record Game(
    String  Console,
    String  GameName,
    String  Review,
    Integer Score
) { public static final String FileName = "Games.csv"; }
