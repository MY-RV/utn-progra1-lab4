
import App.Bootapp;
import App.Views.ReportChart;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author MYRV
 */
public class Runable {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Bootapp();
        var chart = new ReportChart(true);
        chart.setVisible(true);
    }
    
}
