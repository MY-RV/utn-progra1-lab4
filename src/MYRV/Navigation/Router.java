package MYRV.Navigation;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

import MYRV.Config;
import javax.swing.JFrame;

/**
 *
 * @author MYRV
 */
public class Router {
    private static JFrame[] Views;
    
    public static void setViews(JFrame ...views) {
        Views = views;
    }
    
    public static JFrame goTo(String name) {
        JFrame redirection = null;
        for (var view : Views) {
            var viewName = view.getClass().getName().replace(Config.ViewsPackage()+".","");
            var match = name.equals(viewName);
            view.setVisible(match);
            if (match) redirection = view;
        }
        return redirection;
    }
}
