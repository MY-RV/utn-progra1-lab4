/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package MYRV.FileTreatment.Ext;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MYRV
 * @param <T>
 */
public abstract class BDataTreatCSV<T> {
    public List<T> rows = new ArrayList<>();
    public String[] headers;

    public abstract void addRow(String args[]);

    public void addRow(String str) {
        addRow(Parser.CsvLine.from(str));
    }
}
