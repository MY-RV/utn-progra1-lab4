/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MYRV.FileTreatment;

import MYRV.DataTreatment.RcdMaker;
import MYRV.FileTreatment.Ext.BFileTreat;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.regex.Pattern;
import java.util.*;
import java.io.*;

/**
 *
 * @author MYRV
 * @param <T>
 */
public class TrtCSV<T> extends BFileTreat<TrtCSV, TrtCSV.Data>{
    public interface Condition<T> { boolean check(T rcd); }
    private final RcdMaker<T> Maker;
    
    public static <T>TrtCSV<T> For(RcdMaker<T> Maker, String FilePath) {
        return new TrtCSV(Maker, FilePath);
    }
    
    private TrtCSV(RcdMaker<T> Maker, String FilePath) {
        super(FilePath);
        this.Maker = Maker;
    }

    @Override
    public Data<T> getData() {
        var response = new Data(Maker);
        System.out.println(this.FilePath());
        try (var file = new Scanner(new File(this.FilePath()))) {
            response.headers = Parser.csvLine(file.nextLine());
            while (file.hasNextLine()) response.addRow(file.nextLine());
            file.close();
            return response;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TrtCSV.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public void addData() {}    
    
    public void putData(Condition condition, T rcd) {
        var data = getData();
        try (var file = new BufferedWriter(new FileWriter(FilePath()))) {
            var headers = Arrays.toString(data.headers);
            file.write(headers.substring(1, headers.length()-1) + "\r");
            for (var row : data.rows) {
                var pass = false;
                try { pass = condition.check(row);} 
                catch (Exception e) { }
                var map = Maker.toHashMap((pass) ? rcd : row);
                file.write(newLine(data.headers, map));
            }
            file.close();
        } catch (IOException ex) {
            Logger.getLogger(TrtCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private String newLine(String[] headers, HashMap<String, Object> map) {
        var response = "";
        for (var header : headers) {
            if (!response.equals("")) response += ",";
            var value = String.valueOf(map.get(header));
            if (value.split(",").length == 1) response += value;
            else response += "\"" + value + "\"";
        }
        return response+"\r";
    }
    
    private static class Parser {
        private static final Parser parser = new Parser();
        private final ArrayList<String> matches = new ArrayList<>();    
        private final Pattern pattern = Pattern.compile("\"([^\"]*)\"|(?<=,|^)([^,]*)(?:,|$)");

        public static String[] csvLine(String csvLine){
            return parser.arrFrom(csvLine);
        }

        private String[] arrFrom(String csvLine) {
            var matcher = pattern.matcher(csvLine);
            matches.clear();
            String match;
            while (matcher.find()) {
                match = matcher.group(1);
                matches.add((match!=null) 
                    ? match.strip() 
                    : matcher.group(2).strip());
            }
            if (matches.isEmpty()) return new String[0];
            return matches.toArray(String[]::new);
        }
    }
    
    public static class Data<T> {
        private final RcdMaker<T> Maker;
        public String[] headers;
        public List<T> rows = new ArrayList<>();

        protected Data(RcdMaker<T> maker) { this.Maker = maker; }

        public void addRow(String str) { addRow(Parser.csvLine(str)); }
        public void addRow(String[] args) {
            this.rows.add(Maker.create((r) -> {
                for (int i = 0; i < headers.length; i++) {
                    r.set(headers[i], args[i]);
                }
            }));
        }
    }

}
