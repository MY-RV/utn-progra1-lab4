package MYRV;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author MYRV
 */
public class Config {
    public final static String Separator = System.getProperty("file.separator");
    public final static String AppDir = System.getProperty("user.dir")+Separator;
    
    private static String DataDir = "";
    public static void DataDir(String dir) { DataDir = castSeparator(dir); }
    public static String DataDir() { return DataDir; }
    
    private static String ViewsPackage = "";
    public static void ViewsPackage(String pack) { ViewsPackage = pack; }
    public static String ViewsPackage() { return ViewsPackage; }
    
    public static String castSeparator(String path) {
        return path.replace('\\', '/').replace("/", Separator);
    }
}
