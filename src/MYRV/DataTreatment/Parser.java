/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MYRV.DataTreatment;

import java.util.HashMap;
import java.util.UUID;

/**
 *
 * @author MYRV
 */
public class Parser {
    private static interface parser { public Object parse(Object value); }
    private static String str(Object obj) { return String.valueOf(obj); }
    private static final HashMap<Class, parser> PMap = new HashMap<>(){{
        put(String.class,  (value) -> String.valueOf(value));
        put(Integer.class, (value) -> Integer.valueOf(str(value)));
        put(Boolean.class, (value) -> Boolean.valueOf(str(value)));
        put(UUID.class,    (value) -> UUID.fromString(str(value)));
    }};
    
    public static <T>T For(Class<T> key, Object value) {
        try {
            return (T) PMap.get(key).parse(value);
        } catch (Exception e) {
            return null;
        }
    }
    
}
