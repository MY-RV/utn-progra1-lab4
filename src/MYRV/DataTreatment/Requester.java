/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MYRV.DataTreatment;

import MYRV.Config;
import MYRV.FileTreatment.Ext.BDataTreatCSV;
import MYRV.FileTreatment.TrtCSV;
import java.util.List;
//import MYRV.FileTreatment.Writers.CSVW;

/**
 *
 * @author MYRV
 * @param <Mdl>
 */
public class Requester<Mdl> {    
    private final TrtCSV<Mdl> Treat;
    private final Class<Mdl> Model;
    private final String FileName;
    
    public final static <Mdl>Requester<Mdl> For(Class<Mdl> Model) {
        return new Requester(Model, getFileName(Model));
    }
    private Requester(Class<Mdl> Model, String FileName) {
        this.Treat = TrtCSV.For(RcdMaker.For(Model), FileName);
        this.FileName = FileName;
        this.Model = Model;
        
        this.Treat.withPrefix(() -> Config.DataDir());
    }
    
    private static String getFileName(Class<?> Model) {
        try {
            return (String) Model.getField("FileName").get(null); 
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            return null; 
        } // Logger.getLogger(Querier.class.getName()).log(Level.SEVERE, null, ex);
    }
    public String FilePath() { return Config.DataDir() + FileName; }
    
    public List<Mdl> get() { return Treat.getData().rows; }
    
    public void update(Mdl input, TrtCSV.Condition condition) {
        Treat.putData(condition, input);
    }
    
}
