/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MYRV.DataTreatment;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MYRV
 * @param <Rcd> Used as Record type
 */
public class RcdMaker<Rcd> {
    private final ValueMapper valMap = new ValueMapper();
    private final Class<Rcd> rcdClass;
    private final Constructor constr;
    private final Parameter[] params;
    public interface Callback{ abstract void run(ValueMapper obj); }
        
    private RcdMaker(Class<Rcd> RecordClass) {
        this.rcdClass = RecordClass;
        this.constr = rcdClass.getConstructors()[0];
        this.params = constr.getParameters();
    }
    public static <Rcd>RcdMaker<Rcd> For(Class<Rcd> RecordClass) {
        return new RcdMaker(RecordClass);
    }
        
    public Rcd create(Callback builder) {
        builder.run(this.valMap);
        var args = this.buildParams();
        this.valMap.clean();
        return newInstance(args);
    }
    public Rcd update(Rcd rcd, Callback builder) {
        builder.run(this.valMap);
        var args = this.buildParams(rcd);
        this.valMap.clean();
        return newInstance(args);
    }
    
    private Rcd newInstance(Object args[]) {
        try {
            return (Rcd) constr.newInstance(args);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(RcdMaker.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    private Object[] buildParams(Object rcd) {
        var response = new Object[params.length];
        for (int x = 0; x < response.length; x++) {
            var param = params[x];
            var value = this.valMap.get(param.getName());
            if (value == null) value = this.runMethod(rcd, param.getName());
            response[x] = Parser.For(param.getType(), value);
        } return response;
    }
    private Object[] buildParams() {
        var response = new Object[params.length];
        for (int x = 0; x < response.length; x++) {
            var param = params[x];
            var value = this.valMap.get(param.getName());
            response[x] = Parser.For(param.getType(), value);
        } return response;
    }
    
    public static class ValueMapper {
        private HashMap<String, Object> valMap = new HashMap<>();
        protected Object get(String key) { return this.valMap.get(key); }
        protected void clean() { this.valMap = new HashMap<>(); }
        public ValueMapper set(String key, Object value) {
            valMap.put(key, value);
            return this;
        }
    }
    
    public HashMap<String, Object> toHashMap(Rcd rcd) {
        HashMap<String, Object> response = new HashMap();
        for (Parameter param1 : params) {
            java.lang.String param = param1.getName();
            response.put(param, this.runMethod(rcd, param));
        } return response;
    }
    
    //<editor-fold defaultstate="collapsed" desc="TO REFACTOR">
    private Method getMethod(Object obj, String name) {
        try {
            return obj.getClass().getMethod(name);
        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(RcdMaker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private Object runMethod(Object obj, String name) {
        try {
            return this.getMethod(obj, name).invoke(obj);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(RcdMaker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    //</editor-fold>

}
